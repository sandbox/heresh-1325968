<?php

/**
 * @file comment.tpl.php
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: Body of the post.
 * - $date: Date and time of posting.
 * - $links: Various operational links.
 * - $new: New comment marker.
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $submitted: By line with date and time.
 * - $title: Linked title.
 *
 * These two variables are provided for context.
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * @see template_preprocess_comment()
 * @see theme_comment()
 */
?>
<div class="<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status ?> clear-block">
	<?php if($picture): ?>
	<div class="avatarHolder">
		<?php print $picture; ?>
	</div>
	<?php endif; ?>

  <?php if ($comment->new): ?>
    <span class="new"><?php print $new ?></span>
  <?php endif; ?>


	
  <div class="content comment">
		<div class="commentAuthorAndDate">
			<div class="commentAuthor">
					<strong>
						<?php print $comment->name; ?>
					</strong>
					<em><?php print t('می گه :'); ?></em>
			</div>
			<div class="commentDate">
				<?php print $date; ?>
			</div>
		</div><!-- commentAuthorAndDAte -->
		
		<div class="commentText">
    	<?php print $content ?>
		
		  <?php if ($signature): ?>
		  <div class="user-signature clear-block">
		    <?php print $signature ?>
		  </div>
		  <?php endif; ?>

			<?php print $links; ?>
				
		</div><!-- commentText -->
  </div><!-- comment content -->


</div>
