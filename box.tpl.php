<?php

/**
 * @file box.tpl.php
 *
 * Theme implementation to display a box.
 *
 * Available variables:
 * - $title: Box title.
 * - $content: Box content.
 *
 * @see template_preprocess()
 */
?>
<div id="commentForm" class="box">
	<div class="subTitle">
		<div class="subTitleBorder">
			<?php if ($title): ?>
				<h4><?php print $title ?></h4>
			<?php endif; ?>
		</div>
	</div>

  <div class="content"><?php print $content ?></div>
</div>
