<?php ?>

<div id="node-<?php print $node->nid; ?>" class="post node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">

	<div class="mainTitle">
  	<h3><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h3>
		<div class="postDate">			
			<span class="day"><?php print $date_day ?></span>
			<span class="month"><?php print $date_month ?></span>
			<span class="year"><?php print $date_year ?></span>
		</div>
	</div>

  <div class="meta">
	<?php if($submitted || isset($node->links['comment_comments']) || isset($node->links['comment_add'])): ?>
    <span class="submitted postMeta postAuthorAndComments">
  	<?php print $submitted; ?>
		<?php if(isset($node->links['comment_comments'])){ ?> 
			<p class="commentNumber"><a class="comment" href="<?php print $node->links['comment_comments']['href']?>"><?php print $node->links['comment_comments']['title'] ?></a></p> 
		<?php }elseif (isset($node->links['comment_add'])){ ?>
			<p class="commentNumber"><a class="comment" href="<?php print $node->links['comment_add']['href']?>"><?php print $node->links['comment_add']['title'] ?></a></p> 
		<?php } ?>
		</span>
  <?php endif; ?>

  </div>

  <div class="content entry">
    <?php print $content ?>
  </div>

  <a href="<?php print $node->links['node_read_more']['href'] ?> " ><?php print $node->links['node_read_more']['title'] ?></a>
		<?php if ($terms): ?>
    <div class="terms terms-inline postMeta postTags"><p>
			<strong>برچسب ها :</strong>
			<?php print $terms ?></p>
		</div>
		<?php endif; ?>
</div>
