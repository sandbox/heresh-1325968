<?php

/**
 *
 */
?>
	<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block block-<?php print $block->module ?>">
	<?php //if ($block->subject): 
?>

				<div class="subTitle">
					<h4><strong><?php print $block->subject; ?></strong></h4>
					<div class="leftFold"></div>
					<div class="rightFold"></div>
				</div>
	<?php //endif;
?>

		<div class="content">
		  <?php print $block->content ?>
		</div>
	</div>
</div>
