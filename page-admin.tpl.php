<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl" lang="fa-IR">

	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><?php print $head_title ?></title>
		<?php print $head ?>
		<?php print $styles ?>
		<?php print $scripts ?>
		<!--[if IE]>
   		<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/IE.css";</style>
		<!--<![endif]-->
	</head>

	
	<body class="rtl home blog">
		
		<div id="header">
			<div id="subscribe">
				<?php print $feed_icons; ?>
			</div>
			<div class="pageList">
				<?php if (isset($primary_links)) {
  				print theme("links", $primary_links, array("class" => "navmenu primary-links"));
				} ?>
			</div>
		<!-- header -->
		</div>

		<div id="mainWrap">
			<div id="main">
				<div id="siteDescription">
					<h2><a href="<?php print check_url($front_page) ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h2>
				</div>
				<div id="content">
		
					 <?php if ($mission): ?>
				    <div id="mission"><?php print $mission; ?></div>
				    <?php endif; ?>

				    <?php print $highlight; ?>
				    <?php print $messages; ?>
				    <?php if ($tabs): ?>
				      <div class="tabs"><?php print $tabs; ?></div>
				    <?php endif; ?>
				    <?php print $help; ?>

					<div class="entry"><?php print $content; ?>	</div>
				</div>
				<div id="sidebar">
							<?php print $sidebar_right;?>
				</div>

			<div id="content-Bottom">
				<?php print $content_bottom; ?>
				<div class="page-Bottom"></div>
			</div>
		</div><!--main -->
	</div><!--mainwarper -->
	<div id="footer-warper">
		<div class="footer">
			<?php print $footer; ?>
		</div>
		<div id="footer-message"><p>
				<?php print $footer_message; ?></p>
		</div>
	</div>

</body>
</html>
