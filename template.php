<?php

function spectrum_preprocess_node(&$vars){
	$node = $vars['node'];
	$vars['date_day'] = format_date($node->created, 'custom', 'j');
	$vars['date_month'] = format_date($node->created, 'custom', 'M');
	$vars['date_year'] = format_date($node->created, 'custom', 'Y');
}

function spectrum_node_submitted($node) {
  return '<p class="author">' . t('نویسنده: <strong> !username </strong>', 
    array(
    '!username' => theme('username', $node), 
    '@datetime' => format_date($node->created),
  )) . '</p>';
}

function spectrum_feed_icon($url, $title) {
  if ($image = theme('image', 'misc/feed.png', t('Syndicate content'), $title)) {
    return '<a title="' . $title .'" href="' . check_url($url) . '" class="feed-icon"></a>';
  }
}

function spectrum_menu_tree($tree) {
  return '<div id="blogrollBox" class="sidebarBox"><ul class="menu">' . $tree . '</ul></div>';
}





